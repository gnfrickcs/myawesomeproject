﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region AudioManagerSingleton
    public static AudioManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public AudioClip music;
    public AudioClip explosion;
    
    public AudioSource generalSpeaker;
    
    public void OneShotAudio(AudioClip clip, AudioSource source)
    {
        if (source == null)
        {
            source = Camera.main.gameObject.GetComponent<AudioSource>();
        }
        
        source.PlayOneShot(clip);
    }

    public void BackgroundMusic(AudioClip clip, AudioSource source)
    {
        if (source == null)
        {
            source = Camera.main.gameObject.GetComponent<AudioSource>();
        }
        
        source.clip = clip;
        source.Play();      
    }

    public void StopPlaying(AudioSource source)
    {
        source.Stop();
    }
}
