﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public int bulletAmount;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<MovementPlayer>() != null)
        {
            if (!other.gameObject.GetComponent<MovementPlayer>().isShooter)
            {
                other.gameObject.GetComponent<MovementPlayer>().isShooter = true;
                other.gameObject.GetComponent<MovementPlayer>().shooterAmmo = bulletAmount;
            }
        }
    }
}
