﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AutoDestroy : MonoBehaviour
{
    public bool destroyable;
    public bool isParticle;
    public bool isObstacle;

    private float _time;
    private ParticleSystem[] _systems;

    private void Awake()
    {
        if (isParticle)
        {
            _systems = gameObject.GetComponentsInChildren<ParticleSystem>();

            if (_systems == null)
            {
                _systems = gameObject.GetComponents<ParticleSystem>();
            }
            
            _time = GetHigherDuration();
        }
    }

    private void Update()
    {
        if (destroyable)
        {
            Timer();            
        }
    }

    void Timer()
    {
        _time -= Time.deltaTime;

        if (_time < 0)
        {
            Destroy(gameObject);
        }
    }

    float GetHigherDuration()
    {
        float actualTime = 0f;
        float newTime = _systems[0].main.duration;

        foreach (var p in _systems)
        {
            actualTime = p.main.duration;
            if (newTime < actualTime)
            {
                newTime = actualTime;
            }
        }

        return actualTime;
    }

    private void OnCollisionEnter(Collision col)
    {
        if (isObstacle && !destroyable && col.gameObject.GetComponent<MovementPlayer>() == null)
        {
            Destroy(col.gameObject);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (isObstacle && !destroyable && col.gameObject.GetComponent<MovementPlayer>() == null)
        {
            Destroy(col.gameObject);
        }
    }
}
