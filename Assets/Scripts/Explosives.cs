﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosives : MonoBehaviour
{
    public GameObject visualFeedback;
    public AudioClip soundFeedback;

    public Transform startingPos;

    public bool hazard;
    public bool destroyable;

    private void OnTriggerEnter(Collider col)
    {      
        if (col.gameObject.GetComponentInParent<Bullet>() != null)
        {
            col.gameObject.GetComponentInParent<Bullet>().rebounds--;
            Instantiate(visualFeedback, transform.position, transform.rotation);
            AudioManager.Instance.OneShotAudio(soundFeedback, AudioManager.Instance.generalSpeaker);
        }
     
        if (hazard && col.gameObject.GetComponent<MovementPlayer>() != null)
        {
            col.transform.position = startingPos.position;
            col.GetComponent<MovementPlayer>().lives--;

            GameManager.Instance.ResetEnemies();
            GameManager.Instance.GetEnemyCount();
            
            Instantiate(visualFeedback, transform.position, transform.rotation);
            AudioManager.Instance.OneShotAudio(soundFeedback, AudioManager.Instance.generalSpeaker);
        }

        if (destroyable)
        {
            Destroy(gameObject);
        }
    }
}
