﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float life;
    public int rebounds;

    void Update ()
    {
        gameObject.transform.Translate(0f, 0f, speed * Time.deltaTime);
        life -= Time.deltaTime;

        if (rebounds <= 0)
        {
            Destroy(gameObject);
        }

        if (life <= 0f)
        {
            Destroy(gameObject);
        }
	}
}
