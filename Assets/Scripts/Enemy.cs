﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform startingPos;
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<MovementPlayer>() != null)
        {
            col.transform.position = startingPos.position;
            col.GetComponent<MovementPlayer>().lives--;
            
            GameManager.Instance.ResetEnemies();
            GameManager.Instance.GetEnemyCount();
        }
        
        if (col.gameObject.GetComponentInParent<Bullet>() != null)
        {
            GameManager.Instance.EnemyDestroyed();
            gameObject.SetActive(false);
        }
    }
}
