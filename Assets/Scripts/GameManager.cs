﻿using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager :  MonoBehaviour
{
	#region GameManagerSingleton Declaration
	public static GameManager Instance;

	void Awake()
	{
		if (Instance == null) 
		{
			Instance = this;
			DontDestroyOnLoad (gameObject);
		} 
		else 
		{
			Destroy (gameObject);
		}
	}
	#endregion
	
	private GameObject _uiWon;
    private GameObject _uiLoose;
	private int _enemyCount;
	private GameObject[] _enemies;

    public void SetUI()
	{
		_uiWon = GameObject.FindGameObjectWithTag("CanvasWin");
		_uiWon.gameObject.SetActive(false);
        _uiLoose = GameObject.FindGameObjectWithTag("CanvasLoose");
        _uiLoose.gameObject.SetActive(false);
    }

    private void Start()
    {
	    GetEnemyCount();
    }

    public void GetEnemyCount()
	{
		_enemies = GameObject.FindGameObjectsWithTag("Enemy");
		_enemyCount = _enemies.Length;
	}
	
	private void Update()
	{
        //ForDebugingEnemies
        //print(_enemyCount);

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<MovementPlayer>().lives <= 0f)
        {
            _uiLoose.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

		if (_enemyCount <= 0)
		{
			_uiWon.gameObject.SetActive(true);
			Time.timeScale = 0f;
		}

		if (Time.timeScale <= 0f && Input.GetKeyDown(KeyCode.R))
		{
			Time.timeScale = 1f;
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	public void ResetEnemies()
	{
		foreach (var t in _enemies)
		{
			if (t != null)
			{
				t.gameObject.SetActive(true);
			}
		}
	}

	public void EnemyDestroyed()
	{
		_enemyCount--;
	}
}
