﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Security.Permissions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class MovementPlayer : MonoBehaviour {

	public float movSpeed;
    public float rotSpeed;
    public float shootInterval;

    public int lives;

    public Text bulletInfo;
    public Text lifeInfo;
    
    public GameObject bullet;
    public Transform refPosCen;
    public Transform refPosLeft;
    public Transform refPosRight;

    [HideInInspector]
    public bool isShooter;

    [HideInInspector]
    public int shooterAmmo;

    private float _shootInterval;
    
    private void Start()
    {
        GameManager.Instance.SetUI();
        GameManager.Instance.GetEnemyCount();
        _shootInterval = shootInterval;
        AudioManager.Instance.BackgroundMusic(AudioManager.Instance.music, AudioManager.Instance.generalSpeaker);
    }

    void Update()
    {
        #region InformingUI
        bulletInfo.text = "BULLETS: " + shooterAmmo.ToString();
        lifeInfo.text = "LIVES: " + lives.ToString();
        #endregion

        #region Movement
        if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.Translate(0f, 0f, movSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            gameObject.transform.Translate(0f, 0f, -movSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            gameObject.transform.Rotate(0f, rotSpeed * Time.deltaTime, 0f);
        }

        if (Input.GetKey(KeyCode.A))
        {
            gameObject.transform.Rotate(0f, -rotSpeed * Time.deltaTime, 0f);
        }
        #endregion
        
        #region Shooting
        if (shooterAmmo <= 0f)
        {
            isShooter = false;
        }
        
        if (isShooter)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.yellow;

            if (Input.GetMouseButtonDown(0) && _shootInterval <= 0f)
            {
                //Instantiate(bullet, refPosCen.transform.position, refPosCen.transform.rotation);
                Shoot(refPosCen);
                
                shooterAmmo--;
                _shootInterval = shootInterval;
            }
            
            if (Input.GetMouseButtonDown(1) && _shootInterval <= 0f && shooterAmmo >= 3)
            {
                //Instantiate(bullet, refPosCen.transform.position, refPosCen.transform.rotation);
                //Instantiate(bullet, refPosLeft.transform.position, refPosLeft.transform.rotation);
                //Instantiate(bullet, refPosRight.transform.position, refPosRight.transform.rotation);
                Shoot(refPosCen);
                Shoot(refPosLeft);
                Shoot(refPosRight);

                shooterAmmo -= 3;
                _shootInterval = shootInterval;
            }

            _shootInterval -= Time.deltaTime;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;
        }
        #endregion
    }

    private void Shoot(Transform reference)
    {
        Instantiate(bullet, reference.position, reference.rotation);
    }
}
